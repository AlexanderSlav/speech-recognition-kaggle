import os
from os.path import isdir, join
from pathlib import Path
import pandas as pd

# Math
import numpy as np
from scipy.fftpack import fft
from scipy import signal
from scipy.io import wavfile
import librosa

from sklearn.decomposition import PCA

# Visualization
import matplotlib.pyplot as plt
import seaborn as sns
import IPython.display as ipd
# import librosa.display

import plotly.offline as py
py.init_notebook_mode(connected=True)
import plotly.graph_objs as go
import plotly.tools as tls
import pandas as pd

class Wav2img():
    def __init__(self, path, feature_extractor='MelSpectrogram', figsize=(4,4), extension='.wav'):
        self.figsize = figsize
        self.wav_path = path
        targetdir = self.wav_path.split('/')[:-1]
        self.targetdir = '/'.join(targetdir)
        output_file = self.wav_path.split('/')[-1].split(extension)[0]
        self.output_file = self.targetdir + '/' + output_file

        if feature_extractor=='MelSpectrogram':
            self.feature_extractor = 1
        elif feature_extractor=='MFCC':
            self.feature_extractor = 0

    def call(self):
        # MelSpectrogram
        if self.feature_extractor: 
            return self.wav2spec()
        else:
            return self.wav2mfcc()

    def wav2mfcc(self):
        fig = plt.figure(figsize=self.figsize)
        sound, samplerate = librosa.load(self.wav_path)
        mfccs = librosa.feature.mfcc(y=sound, sr=samplerate, n_mfcc=40)
        librosa.display.specshow(mfccs, x_axis='time')
        output_file = self.output_file + '_mfcc'
        plt.imsave('%s.png' % output_file, mfccs)
        plt.close()
        return (output_file + '.png')

    def wav2spec(self):
        fig = plt.figure(figsize=self.figsize)
        samplerate, sound = wavfile.read(self.wav_path)
        _, spectrogram = self.log_specgram(sound, samplerate)
        output_file = self.output_file + '_spec'
        plt.imsave('%s.png' % output_file, spectrogram)
        plt.close()
        return (output_file + '.png')

    def log_specgram(self, audio, sample_rate, window_size=20, step_size=10, eps=1e-10):
        nperseg = int(round(window_size * sample_rate / 1e3))
        noverlap = int(round(step_size * sample_rate / 1e3))
        freqs, _, spec = signal.spectrogram(audio,
                                            fs=sample_rate,
                                            window='hann',
                                            nperseg=nperseg,
                                            noverlap=noverlap,
                                            detrend=False)
        return freqs, np.log(spec.T.astype(np.float32) + eps)
