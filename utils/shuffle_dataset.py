import os
import random
from tqdm import tqdm_notebook
import shutil
from utils.visualization import wav_to_image

def shuffle(source_dir, threshold, target_dir, class_list):
    source_dir_ = os.path.normpath(source_dir)
    delimiter = '\\'

    try:
        for root, subdirs, files in os.walk(source_dir_):
            class_name = root.split(os.sep)[-1]
            if files and class_name in class_list:
                if not os.path.exists(target_dir + class_name):
                    os.mkdir(target_dir + class_name)
                random.shuffle(files)
                taken_files = files[:int(len(files) * threshold)]
                in_class_bar = tqdm_notebook(desc=str(class_name) + ' preparation', 
                          total=len(taken_files),
                          position=1)
                for file in taken_files:
                    source_path = source_dir + delimiter + class_name + delimiter
                    if '.png' not in file:
                        w2i = wav_to_image.Wav2img(source_path + file, 'MelSpectrogram')
                        path = w2i.call()
                    else:
                        path = source_path + file
                    shutil.copyfile(path, target_dir + class_name + delimiter + file.split('.')[:-1][0] + '.png')
                    in_class_bar.update()

    except KeyboardInterrupt:
        print("Exiting loop spectrogram generating.")

    def shuffle_unknown(source_dir, threshold, target_dir):
        source_dir_ = os.path.normpath(source_dir)
        delimiter = '\\'
        stop_list = ['yes', 'no', 'on',
                     'off', 'up', 'down',
                     'left', 'right', 'stop', 'go', '_background_noise_']

        try:
            for root, subdirs, files in os.walk(source_dir_):
                class_name = root.split(os.sep)[-1]
                if files and class_name not in stop_list:
                    
                    if not os.path.exists(target_dir + 'unknown'):
                        os.mkdir(target_dir + 'unknown')
                    random.shuffle(files)
                    taken_files = files[:int(len(files) * threshold)]
                    in_class_bar = tqdm_notebook(desc=str(class_name) + ' preparation', 
                              total=len(taken_files),
                              position=1)
                    for file in taken_files:
                        source_path = source_dir + delimiter + class_name + delimiter
                        if '.png' not in file:
                            w2i = wav_to_image.Wav2img(source_path + file, 'MelSpectrogram')
                            path = w2i.call()
                        else:
                            path = source_path + file
                        shutil.copyfile(path, target_dir + 'unknown' + delimiter + file.split('.')[:-1][0] + '.png')
                        in_class_bar.update()

        except KeyboardInterrupt:
            print("Exiting loop spectrogram generating.")



# def main():
    # class_list = ['down','go','left','no','off','on','right','stop','unknown','up','yes']
    # source_dir = 'data/train/audio/'
    # target_dir = '../truncted_dataset/truncted_dataset_2/'
    # shuffle(source_dir, 0.1, target_dir, class_list)

# main()