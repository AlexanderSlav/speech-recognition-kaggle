import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.utils.data import Dataset
import torchaudio
import pandas as pd
import numpy as np


class AudioDataset(datasets.DatasetFolder):
    def __init__(self, root, loader, feature_extractor=None, preprocessing=None, extensions=None, transform=None,
                 target_transform=None, is_valid_file=None):
        super(datasets.DatasetFolder, self).__init__(root, transform=transform,
                                            target_transform=target_transform)
        classes, class_to_idx = self._find_classes(self.root)
        samples = datasets.folder.make_dataset(self.root, class_to_idx, extensions, is_valid_file)
        if len(samples) == 0:
            raise (RuntimeError("Found 0 files in subfolders of: " + self.root + "\n"
                                "Supported extensions are: " + ",".join(extensions)))
        self.feature_extractor = feature_extractor
        self.preprocessing = preprocessing
        self.loader = loader
        self.transform = transform
        self.extensions = extensions
        self.classes = classes
        self.class_to_idx = class_to_idx
        self.samples = samples
        self.targets = [s[1] for s in samples]


    def __getitem__(self, index):
        path, target = self.samples[index]
        waveform, sr = torchaudio.load(path)
        if self.feature_extractor:
            sample = self.feature_extractor(waveform)
        else:
            sample = torchaudio.transforms.MFCC(sample_rate=16000, n_mfcc=40)(waveform)

        #sample = self.preprocessing(waveform) if self.preprocessing else waveform
        if self.transform:
            # sample = self.transform(np.uint8(sample.permute(1, 2, 0)))

            sample = self.transform(np.uint8(sample.permute(1, 2, 0)))
        return sample, target
    def __len__(self):
        return len(self.samples)